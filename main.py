
table = {
    2:(2.706,6.635),
    3:(4.605,9.210),
    4:(6.251,11.341),
    5:(7.779,13.277),
    6:(9.236,15.086),
    7:(10.645,16.812),
    8:(12.017,18.475),
    9:(13.362,20.090),
    10:(14.684,21.666),
    11:(15.987,23.209),
    12:(17.275,24.725),
    13:(18.549,26.217),
    14:(19.812,27.688),
    15:(21.064,29.141),
    16:(22.307,30.578),
    17:(23.542,32.000),
    18:(24.769,33.409),
    19:(25.989,34.805),
    20:(27.204,36.191)
}

chi_Square = 0
T = eval(input('Set T: '))
dice_Face = {}
selected_dice = eval(input('What dice are you rolling? eg. 12 \n'))
if isinstance(selected_dice, int) == False or selected_dice > 20:
    print('Pick a number between 1 and 20 please.')

print(f'Lets roll the dice {str(selected_dice*T)} times')

for item in range(1,selected_dice + 1):
    dice_Face.update({item:0})

for item in range(1,selected_dice*T + 1):
    roll = False
    while roll == False:
        try:
            roll = eval(input(f'Roll:{item}  \n'))
        except:
            pass
    try:
        dice_Face[roll] = dice_Face[roll] + 1



for key in dice_Face:
    chi_Square += ((dice_Face[key] - T)**2)

chi_Square = chi_Square / 2

print(dice_Face)
print(chi_Square)

print(f'{chi_Square}, {table[selected_dice][0]}, {table[selected_dice][1]}')
if chi_Square < table[selected_dice][0]:
    print("This dice appears to be fair.")

if chi_Square > table[selected_dice][1]:
    print('Dice is monst likey BIASED.')

if chi_Square > table[selected_dice][0] and chi_Square < table[selected_dice][1]:
    print('this dice is to close to call.')


